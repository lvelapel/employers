CREATE TABLE logging_user
(
  id INT PRIMARY KEY NOT NULL,
  user_id INT NOT NULL,
  login_time TIMESTAMP NOT NULL,
  logout_time TIMESTAMP NOT NULL
);
CREATE TABLE rabotnik
(
  id INT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(50) NOT NULL,
  sphere_id INT NOT NULL,
  user_id INT
);
CREATE TABLE rabotodatel
(
  id INT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(50) NOT NULL,
  sphere_id INT NOT NULL,
  user_id INT
);
CREATE TABLE sphere
(
  id INT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL
);
CREATE TABLE spheres_id_seq
(
  sequence_name VARCHAR NOT NULL,
  last_value BIGINT NOT NULL,
  start_value BIGINT NOT NULL,
  increment_by BIGINT NOT NULL,
  max_value BIGINT NOT NULL,
  min_value BIGINT NOT NULL,
  cache_value BIGINT NOT NULL,
  log_cnt BIGINT NOT NULL,
  is_cycled BOOL NOT NULL,
  is_called BOOL NOT NULL
);
CREATE TABLE user_role
(
  id INT PRIMARY KEY NOT NULL,
  authority VARCHAR(255) NOT NULL
);
CREATE TABLE user_user_role
(
  user_id INT NOT NULL,
  user_role_id INT NOT NULL,
  id INT PRIMARY KEY NOT NULL
);
CREATE TABLE users
(
  id INT PRIMARY KEY NOT NULL,
  username VARCHAR(20) NOT NULL,
  password VARCHAR(100) NOT NULL,
  username VARCHAR(30) NOT NULL
);
CREATE TABLE users_id_seq
(
  sequence_name VARCHAR NOT NULL,
  last_value BIGINT NOT NULL,
  start_value BIGINT NOT NULL,
  increment_by BIGINT NOT NULL,
  max_value BIGINT NOT NULL,
  min_value BIGINT NOT NULL,
  cache_value BIGINT NOT NULL,
  log_cnt BIGINT NOT NULL,
  is_cycled BOOL NOT NULL,
  is_called BOOL NOT NULL
);
ALTER TABLE logging_user ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX fki_logging_user_user_id__users_id ON logging_user (user_id);
ALTER TABLE rabotnik ADD FOREIGN KEY (sphere_id) REFERENCES sphere (id);
ALTER TABLE rabotnik ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX fki_rabotnik_sphere_id__spheres_id ON rabotnik (sphere_id);
CREATE INDEX fki_rabotnik_user_id__users_id ON rabotnik (user_id);
ALTER TABLE rabotodatel ADD FOREIGN KEY (sphere_id) REFERENCES sphere (id);
ALTER TABLE rabotodatel ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX fki_rabotodatel_sphere_id__spheres_id ON rabotodatel (sphere_id);
CREATE INDEX fki_rabotodatel_user_id__users_id ON rabotodatel (user_id);
CREATE UNIQUE INDEX spheres_name_key ON sphere (name);
ALTER TABLE user_user_role ADD FOREIGN KEY (user_role_id) REFERENCES user_role (id);
ALTER TABLE user_user_role ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX "fki_user_user_role.user_role_id__user_role.id" ON user_user_role (user_role_id);
CREATE INDEX fki_user_user_role_user_id__users_id ON user_user_role (user_id);
CREATE UNIQUE INDEX users_email_key ON users (username);
CREATE UNIQUE INDEX users_password_key ON users (password);
CREATE UNIQUE INDEX users_username_key ON users (username);
