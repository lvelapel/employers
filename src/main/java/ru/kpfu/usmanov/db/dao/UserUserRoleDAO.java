package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.usmanov.db.entities.UsersEntity;

public interface UserUserRoleDAO extends JpaRepository<UsersEntity, Integer> {
}
