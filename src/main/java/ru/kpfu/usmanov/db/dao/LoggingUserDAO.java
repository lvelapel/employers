package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.LoggingUserEntity;

@Repository
public interface LoggingUserDAO extends JpaRepository<LoggingUserEntity, Integer> {
}
