package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.UsersEntity;

import java.util.List;

//@Transactional(readOnly=true)
@Repository
public interface UsersDAO extends JpaRepository<UsersEntity, Integer> {


    UsersEntity findByUsername(String username);

    @Query(" update UsersEntity set enabled = true where id= ?1")
    void setUsernameBlockBy( int user_id);


    List<UsersEntity> findAll();

    @Query(" delete  from UsersEntity  where id= ?1")
    void deleteUserBy( int user_id);
}
