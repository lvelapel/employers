package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.UserRoleEntity;

@Repository
public interface UserRoleDAO extends JpaRepository<UserRoleEntity, Integer> {
    UserRoleEntity findByAuthority(String s);
}
