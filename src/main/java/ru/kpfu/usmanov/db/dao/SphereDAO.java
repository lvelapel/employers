package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.SphereEntity;

@Repository
public interface SphereDAO extends JpaRepository<SphereEntity, Integer> {
}
