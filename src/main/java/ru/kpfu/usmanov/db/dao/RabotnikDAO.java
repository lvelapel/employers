package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.RabotnikEntity;

import java.util.List;

@Repository
public interface RabotnikDAO extends JpaRepository<RabotnikEntity, Integer> {
    List<RabotnikEntity> findAll();
    List<RabotnikEntity> findByNameAndSurnameAndSphereBySphereId_Name(
            String name,
            String surname,
            String sphere_name);


}
