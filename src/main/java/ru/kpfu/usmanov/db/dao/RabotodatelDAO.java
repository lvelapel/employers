package ru.kpfu.usmanov.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.usmanov.db.entities.RabotodatelEntity;

import java.util.List;


@Repository
public interface RabotodatelDAO extends JpaRepository<RabotodatelEntity, Integer> {
    List<RabotodatelEntity> findAll();

    List<RabotodatelEntity> findByNameAndSurnameAndSphereBySphereId_Name(String name, String surname, String sphere_name);
}
