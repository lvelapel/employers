package ru.kpfu.usmanov.db.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_user_role", schema = "public", catalog = "prog_sem2")
public class UserUserRoleEntity {
    private UserRoleEntity userRoleByUserId;
    private UsersEntity usersByUserId;
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UsersEntity getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(UsersEntity usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "user_role_id", referencedColumnName = "id", nullable = false)
    public UserRoleEntity getUserRoleByUserId() {
        return userRoleByUserId;
    }

    public void setUserRoleByUserId(UserRoleEntity userRoleByUserId) {
        this.userRoleByUserId = userRoleByUserId;
    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserUserRoleEntity that = (UserUserRoleEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
