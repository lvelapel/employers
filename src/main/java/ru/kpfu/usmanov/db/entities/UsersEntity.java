package ru.kpfu.usmanov.db.entities;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kpfu.usmanov.util.FieldMatch;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "passwordRepeat", message = "The password fields must match")
})
@DynamicUpdate

@Entity
@Access(AccessType.FIELD)
@Table(name = "users", schema = "public", catalog = "prog_sem2")
public class UsersEntity implements UserDetails {
    @Id
    @Column(name = "id", updatable=false, nullable=false)
    private int id;

    @Length(max = 255)
    private String username;
    private String password;
    private String email;
    private boolean enabled;

    //TODO пойми подробнее
    @ManyToMany(fetch = FetchType.EAGER, cascade={CascadeType.MERGE}, targetEntity = UserRoleEntity.class)
    @JoinTable( 
            name = "user_user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "user_role_id")
    )

//    @ElementCollection(targetClass=UserRoleEntity.class)
    private Set<UserRoleEntity> roles = new HashSet<>();

    public UsersEntity(String username) {
        this.username=username;
    }

    public UsersEntity() {

    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false, insertable = true, updatable = true, length = 20)
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Basic
    @Column(name = "password", nullable = false, insertable = true, updatable = true, length = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email", nullable = false, insertable = true, updatable = true, length = 30)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != that.id) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    public void addRole(UserRoleEntity role_user) {
        this.roles.add(role_user);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    //TODO
//    public void addRole(Role roleName) {
//        this.roles.add(roleName)
//    }
}
