package ru.kpfu.usmanov.db.entities;

import javax.persistence.*;

@Entity
@Table(name = "rabotodatel", schema = "public", catalog = "prog_sem2")
public class RabotodatelEntity {
    private int id;
    private String name;
    private String surname;
    private SphereEntity sphereBySphereId;
    private UsersEntity usersByUserId;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname", nullable = false, insertable = true, updatable = true, length = 50)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RabotodatelEntity that = (RabotodatelEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "sphere_id", referencedColumnName = "id", nullable = false)
    public SphereEntity getSphereBySphereId() {
        return sphereBySphereId;
    }

    public void setSphereBySphereId(SphereEntity sphereBySphereId) {
        this.sphereBySphereId = sphereBySphereId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UsersEntity getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(UsersEntity usersByUserId) {
        this.usersByUserId = usersByUserId;
    }
}
