package ru.kpfu.usmanov.forms;

public class SearchForm {
    private String name;
    private String surname;
    private String sphere_name;
    private boolean isHeRabotodatel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSphere_name() {
        return sphere_name;
    }

    public void setSphere_name(String sphere_name) {
        this.sphere_name = sphere_name;
    }

    public boolean isHeRabotodatel() {
        return isHeRabotodatel;
    }

    public void setIsHeRabotodatel(boolean isHeRabotodatel) {
        this.isHeRabotodatel = isHeRabotodatel;
    }
}
