package ru.kpfu.usmanov.forms;

public class RegForm {
    private String username;
    private String name;
    private String surname;
    private String password;
    private String email;
    private String sphere_name;
    private boolean isIAmRabotodatel;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSphere_name() {
        return sphere_name;
    }

    public void setSphere_name(String sphere_name) {
        this.sphere_name = sphere_name;
    }

    public boolean isIAmRabotodatel() {
        return isIAmRabotodatel;
    }

    public void setIsIAmRabotodatel(boolean isIAmRabotodatel) {
        this.isIAmRabotodatel = isIAmRabotodatel;
    }
}
