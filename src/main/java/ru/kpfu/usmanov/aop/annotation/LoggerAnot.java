package ru.kpfu.usmanov.aop.annotation;

import java.lang.annotation.*;

/**
 * Created by Peter on 05.04.2015.
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoggerAnot {
}
