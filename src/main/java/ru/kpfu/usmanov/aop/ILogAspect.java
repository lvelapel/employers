package ru.kpfu.usmanov.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;

/**
 * Created by Peter on 05.04.2015.
 */
public interface ILogAspect {
    @AfterReturning(value = "@annotation(ru.kpfu.usmanov.aop.annotation.LoggerAnot)", returning = "returnedValueFromMethod")
    public void doAfterReturn(JoinPoint jp, Object returnedValueFromMethod);
}
