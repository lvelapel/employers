package ru.kpfu.usmanov.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.kpfu.usmanov.db.dao.LoggingUserDAO;
import ru.kpfu.usmanov.db.entities.LoggingUserEntity;

import java.sql.Date;

/**
 * Created by Peter on 01.04.2015.
 */
@Component
@Aspect
public class LogAspect implements ILogAspect {

    @Qualifier("loggingUserDAO")
    @Autowired
    private LoggingUserDAO loggingUserDAO;


    @AfterReturning(value = "@annotation(ru.kpfu.usmanov.aop.annotation.LoggerAnot)", returning = "returnedValueFromMethod")
    public void doAfterReturn(JoinPoint jp, Object returnedValueFromMethod) {
        String method = jp.getSignature().toString();
        Date date = new Date(new java.util.Date().getTime());
        String returnedValue = null;
        if (returnedValueFromMethod == null) {
            if (jp.getSignature().toString().substring(0, 4).equals("void")) {
                returnedValue = "void";
            } else {
                returnedValue = "null";
            }
        } else {
            returnedValue = returnedValueFromMethod.toString();
        }
        LoggingUserEntity lue = new LoggingUserEntity();

        loggingUserDAO.save(lue);
    }
}
