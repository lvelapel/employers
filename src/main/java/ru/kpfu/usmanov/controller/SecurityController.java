package ru.kpfu.usmanov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.usmanov.db.dao.UsersDAO;
import ru.kpfu.usmanov.db.entities.UsersEntity;
import ru.kpfu.usmanov.forms.LoginForm;
import ru.kpfu.usmanov.service.ServiceImpl;

import javax.validation.Valid;

@Controller
public class SecurityController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Qualifier("userService")
    @Autowired
    private ServiceImpl service;
    @Qualifier("usersDAO")
    @Autowired
    private UsersDAO usersDAO;

    protected String showRegisterForm(ModelMap map) {
        map.put("userAuthorities", service.findAllRoles());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
//    @PreAuthorize("isAnonymous()")
    public String registration(ModelMap map) {
        map.put("user", new UsersEntity());
        return showRegisterForm(map);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
//    @PreAuthorize("isAnonymous()")
    public String registerHandler(
            RedirectAttributes redirectAttributes,
            @ModelAttribute("users")
            @Valid UsersEntity user,
            BindingResult result,
            ModelMap map
    ) {
        if (!result.hasErrors()) {
            try {
                service.registerUser(user);
                redirectAttributes.addFlashAttribute("message", "Вы успешно зарегистрировались");
                redirectAttributes.addFlashAttribute("messageType", "success");
                return "redirect:" + MvcUriComponentsBuilder.fromPath("/").build();
            } catch (DuplicateKeyException ex) {
                result.rejectValue("username", "entry.duplicate", "Some error");
            }
        }
        return showRegisterForm(map);
    }


    @RequestMapping(value = "/users")
//    @PreAuthorize("hasRole('ADMIN')")
    public String users(ModelMap map){
        map.put("users", usersDAO.findAll());
        return "users";
    }
    @RequestMapping(value = "/block_user", method = RequestMethod.POST)
//    @PreAuthorize("hasRole('ADMIN')")
    public void block_user(@RequestParam("user_id") int userId, ModelMap map){
        usersDAO.setUsernameBlockBy(userId);
//        return "users";
    }
    @RequestMapping(value = "/delete_user", method = RequestMethod.POST)
//    @PreAuthorize("hasRole('ADMIN')")
    public void delete_user(@RequestParam("user_id") int userId, ModelMap map){
        usersDAO.deleteUserBy(userId);
//        return "users";
    }


    @RequestMapping(value = "/login")
//    @PreAuthorize("isAnonymous()")
    public String login(@RequestParam(required = false) String error, @ModelAttribute("loginForm") LoginForm loginForm, BindingResult result, ModelMap map) {
        map.put("Неправильная пара email/пароль!", error);
        return "login";
    }

    // tutor https://raibledesigns.com/rd/entry/implementing_ajax_authentication_using_jquery
    @RequestMapping(value = "/login_ajax", method = RequestMethod.POST)
    public
    @ResponseBody
    LoginStatus loginAjax(@RequestParam String username, @RequestParam String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        UserDetails userDetails = service.loadUserByUsername(username);
        token.setDetails(userDetails);
        if (userDetails != null && passwordEncoder.matches(password, userDetails.getPassword())) {
            Authentication authToken =
                    new UsernamePasswordAuthenticationToken(
                            userDetails,
                            null,
                            userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authToken);
            return new LoginStatus(authToken.isAuthenticated(), authToken.getName());
        } else {
            return new LoginStatus(false, null);
        }
    }

    private class LoginStatus {
        private final boolean loggedIn;
        private final String username;

        public LoginStatus(boolean loggedIn, String username) {
            this.loggedIn = loggedIn;
            this.username = username;
        }

        public boolean isLoggedIn() {
            return loggedIn;
        }

        public String getUsername() {
            return username;
        }
    }
}

