package ru.kpfu.usmanov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.usmanov.service.ServiceImpl;

@Controller
public class MainController {

    @Qualifier("userService")
    @Autowired
    private ServiceImpl service;

    @RequestMapping(value = {"/", "/main"})
//    @PreAuthorize("isAnonymous()")
    public String main(ModelMap map) {
        map.put("rabotniki", service.getRabotniki());
        map.put("rabotodateli", service.getRabotodateli());
        return "main";
    }






}
