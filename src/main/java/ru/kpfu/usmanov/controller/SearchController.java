package ru.kpfu.usmanov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.usmanov.forms.SearchForm;
import ru.kpfu.usmanov.service.ServiceImpl;

import java.util.List;

@Controller
public class SearchController {
    @Qualifier("userService")
    @Autowired
    private ServiceImpl service;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
//    @PreAuthorize("isAnonymous('USER')")
    public String search(ModelMap map) {
        map.put("search-form", new SearchForm());
        return "search";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
//    @PreAuthorize("isAnonymous('USER')")
    public String searchHandler(
            @ModelAttribute("search-form") SearchForm searchForm,
            BindingResult bindingResult, ModelMap map) {

        List rabotniki = service.findRabotnikByFields(searchForm.getName(), searchForm.getSurname(), searchForm.getSphere_name());
        List rabotodateli = service.findRabotodatelByFields(searchForm.getName(), searchForm.getSurname(), searchForm.getSphere_name());
        if (rabotniki.size()!=0){
            map.put("rabotniki", rabotniki);
        }
        if (rabotodateli.size()!=0){
            map.put("rabotodateli", rabotodateli);

        }
        return "search";
    }
}
