package ru.kpfu.usmanov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.usmanov.db.dao.RabotnikDAO;
import ru.kpfu.usmanov.db.dao.RabotodatelDAO;
import ru.kpfu.usmanov.db.dao.UserRoleDAO;
import ru.kpfu.usmanov.db.dao.UsersDAO;
import ru.kpfu.usmanov.db.entities.UserRoleEntity;
import ru.kpfu.usmanov.db.entities.UsersEntity;

import java.util.List;


@Service("ServiceImpl")
public class ServiceImpl implements UserDetailsService {

    @Qualifier("rabotnikDAO")
    @Autowired
    private RabotnikDAO rabotnikDAO;

    @Qualifier("usersDAO")
    @Autowired
    private UsersDAO usersDAO;

    @Qualifier("rabotodatelDAO")
    @Autowired
    private RabotodatelDAO rabotodatelDAO;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Qualifier("userRoleDAO")
    @Autowired
    private UserRoleDAO userRoleDAO;


    public List getRabotniki() {
        return rabotnikDAO.findAll();
    }

    public List getUsers() {
        return usersDAO.findAll();
    }

    public List getRabotodateli() {
        return rabotodatelDAO.findAll();
    }

    public List<UserRoleEntity> findAllRoles() {
        return userRoleDAO.findAll();
    }

    public void registerUser(UsersEntity user) {
        if(usersDAO.findByUsername(user.getUsername()) != null){
            throw new DuplicateKeyException("Duplicate key - username field.");
        }
        user.addRole(userRoleDAO.findByAuthority("ROLE_USER"));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        usersDAO.save(user);
    }

    public List findRabotnikByFields(String name, String surname, String sphere_name) {

        return rabotnikDAO.findByNameAndSurnameAndSphereBySphereId_Name(name, surname, sphere_name);
    }

    public List findRabotodatelByFields(String name, String surname, String sphere_name) {
        return rabotodatelDAO.findByNameAndSurnameAndSphereBySphereId_Name(name,surname,sphere_name);
    }

    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return usersDAO.findByUsername(s);

    }


}
