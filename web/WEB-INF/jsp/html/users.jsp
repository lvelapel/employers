<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">


<head>
    <meta http-equiv="content-type"
          content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <title>Каталог</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css"
          rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top"
     role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="/main">Работнички</a>
        </div>
        <div id="navbar"
             class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">


                <li><a href="/registration">Регистрация</a></li>
                <li><a href="/login">Вход</a></li>

                <li><a href="/logout">Выход</a></li>
            </ul>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1 ">

            <div class="sidebar">
                <ul class="nav nav-sidebar">
                    <li class=""><a href="/search">Поиск работников</a></li>
                    <li><a href="/map">Карта сайта</a></li>
                    <li><a href="/about">О сайте</a></li>
                </ul>

            </div>
        </div>
        <div class="col-sm-11 col-sm-offset-1 col-md-10 col-md-offset-1 main">
            <h1 class="page-header">Список пользователей</h1>


            <div class="table-responsive user-table">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>email</th>
                        <th>Is Enabled?</th>
                        <th>username</th>
                        <th>Block button</th>
                        <th>Delete button</th>
                    </tr>
                    <% int userInd = 1; %>
                    <c:forEach items="${users}" var="user">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.email}</td>
                            <td>${user.enabled}</td>
                            <td>${user.username}</td>
                            <td>

                                <FORM NAME="block_user_form" action="/block_user" METHOD="POST">
                                    <INPUT TYPE="HIDDEN" NAME="user_id" type="number" value="${user.id}">
                                    <input type="submit" value="Block user"
                                           id="btnBlock<%=userInd++%>" onclick=" buttonBlock()">
                                </FORM>
                            </td>
                            <td>

                                <FORM NAME="delete_user_form" action="/delete_user" METHOD="POST">
                                    <INPUT TYPE="HIDDEN" NAME="user_id" type="number" value="${user.id}">
                                    <input type="submit" value="Block user"
                                           id="btnDel<%=userInd%>" onclick="buttonDelete()">
                                </FORM>
                            </td>

                        </tr>
                    </c:forEach>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<SCRIPT LANGUAGE="JavaScript">
    <!--
    function buttonBlock() {
        block_user_form.serealize();
        $('#table-users').after('<div class="text-info">Юзер заблокирован</div>');

    }
    function buttonDelete() {
        block_user_form.serealize();
        $('#table-users').after('<div class="text-info">Юзер удален</div>');

    }
    // -->
</SCRIPT>

</body>
</html>
