<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta http-equiv="content-type"
          content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <title>Каталог</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.css"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css"
          rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top"
     role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="/main">Работнички</a>
        </div>
        <div id="navbar"
             class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">

                <li><a href="/registration">Регистрация</a></li>
                <li><a href="/login">Вход</a></li>
                <li><a href="/logout">Выход</a></li>
            </ul>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="col-md-1 ">

            <div class="sidebar">
                <ul class="nav nav-sidebar">
                    <li class="/search"><a href="#">Поиск работников</a></li>
                    <li><a href="/map">Карта сайта</a></li>
                    <li><a href="/about">О сайте</a></li>
                </ul>

            </div>
        </div>

        <div class="container">

            <div class="row row-centered">
                <div class="col-md-4 col-centered main">


                    <form
                            class="form-signin"
                            role="form"
                            action="/registration"
                            method="post"
                            >
                        <h2 class="form-signin-heading">Регистрация

                            успешна
                            сорвалась
                            <label for="userName"
                                   class="sr-only">Логин</label>
                            <input id="userName"
                                   name="userName"
                                   class="form-control"
                                   placeholder="Логин"
                                   required=""
                                   autofocus=""
                                   type="text">
                            <label for="username"
                                   class="sr-only">Email</label>
                            <input id="username"
                                   name="username"
                                   class="form-control"
                                   placeholder="Email"
                                   required=""
                                   autofocus=""
                                   type="text">

                            <label for="name"
                                   class="sr-only">Имя</label>
                            <input
                                    id="name"
                                    name="name"
                                    class="form-control"
                                    placeholder="Имя"
                                    required=""
                                    type="text">
                            <label for="surname"
                                   class="sr-only">Фамилия</label>
                            <input
                                    id="surname"
                                    name="surname"
                                    class="form-control"
                                    placeholder="Фамилия"
                                    required=""
                                    type="text">

                            <label for="sphere"
                                   class="sr-only">Сфера деятельности</label>
                            <input
                                    id="sphere"
                                    name="sphere"
                                    class="form-control"
                                    placeholder="Сфера деятельности"
                                    required=""
                                    type="text">


                            <label for="password"
                                   class="sr-only">Пароль</label>
                            <input
                                    id="password"
                                    name="password"
                                    class="form-control"
                                    placeholder="Пароль"
                                    required=""
                                    type="password">

                            <div class="radio">
                                <label>
                                    <input type="radio"
                                           name="whoami"
                                           value="customer"
                                           checked>
                                    Я -работник </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio"
                                           name="whoami"
                                           value="employer">
                                    Я - работодатель </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio"
                                           name="role"
                                           value="user"
                                           checked>
                                    Я - простой смертный </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio"
                                           name="role"
                                           value="admin">
                                    Или право имею </label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block"
                                    type="submit">Регистрация

                            </button>
                    </form>

                    <!--</div>-->

                </div>

            </div>
        </div>

    </div>

</div>


</body>
</html>
