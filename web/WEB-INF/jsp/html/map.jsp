
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>
    <meta http-equiv="content-type"
          content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <title>Каталог</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/map.css"
          rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top"
     role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="/main">Работнички</a>
        </div>
        <div id="navbar"
             class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="isAuthenticated()">
                    <li><a href="<c:url value="/logout" />"><i class="fa fa-user"></i> Выход</a></li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li><a href="<c:url value="/login"/>"><i class="fa fa-lock"></i> Login</a></li>
                    <li><a href="<c:url value="/registration"/>"><i class="fa fa-lock"></i> Registration</a></li>
                </security:authorize>
            </ul>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="col-md-1 ">

            <div class="sidebar">
                <ul class="nav nav-sidebar">
                    <li><a href="/search">Поиск работников</a></li>
                    <li class="active"><a href="/map">Карта сайта</a></li>
                    <li><a href="/about">О сайте</a></li>
                </ul>

            </div>
        </div>

        <div class="container">

            <div class="row row-centered">
                <div class="    col-centered main">
                    <h1 class="page-header row-centered">Карта сайта</h1>


                    <h3 class=" row-centered"> Этот сайт создан для того, чтобы работничек и работодатель
                        были вместе! Найдешь любого, кто тебе нужен.
                    </h3>
                </div>
            </div>

        </div>

    </div>
</div>


</body>
</html>
