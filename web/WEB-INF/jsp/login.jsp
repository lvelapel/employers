<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <meta http-equiv="content-type"
          content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <title>Каталог</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.css"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css"
          rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top"
     role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="/main">Работнички</a>
        </div>
        <div id="navbar"
             class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">


                <li><a href="/registration">Регистрация</a></li>
                <li><a href="/login">Вход</a></li>


                <li><a href="/logout">Выход</a></li>

            </ul>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">

        <div class="col-md-1 "></div>
        <div class="col-md-1 ">

            <div class="sidebar">
                <ul class="nav nav-sidebar">
                    <li class=""><a href="/search">Поиск работников</a></li>
                    <li><a href="/map">Карта сайта</a></li>
                    <li><a href="/about">О сайте</a></li>
                </ul>

            </div>
        </div>


        <div class="container">
            <div class="col-md-2"></div>

            <div class="col-md-7 beforeLoginDiv" >
                <div id="loginDiv">
                    <form:form method="post"
                               class="form-signin"
                               role="form"
                               commandName="login"
                               id="loginForm"
                            >
                        <h2 class="form-signin-heading">Вход</h2>

                        <label for="username"
                               id="username-label"
                               class="sr-only">Логин</label>
                        <input id="username"
                               name="username"
                               class="form-control"
                               placeholder="Email address"
                               required=""
                               autofocus=""
                               type="text">
                        <label for="password"
                               class="sr-only">Пароль</label>
                        <input id="password"
                               name="password"
                               class="form-control"
                               placeholder="Password"
                               required=""
                               type="password">


                        <button id="login" class="btn btn-lg btn-primary btn-block"
                                type="submit" >Вход
                        </button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var getHost = function () {
        return 'http://' + window.location.hostname + "8080";
    };

    var loginFailed = function (data, status) {
        $('#username-label').before('<div class="error">Ошибка, залогиньтесь еще раз</div>');
    };

    $("#login").onlive('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: getHost() + "/login_ajax",
            type: "POST",
            data: $("#loginForm").serialize(),
            success: function (data, status) {
                if (data.loggedIn) {
                    $("#loginDiv").remove();
                    // some trash

                    $('#beforeLoginDiv').after('<div class="text-info">Юзер ');
                    $('#beforeLoginDiv').after(data.username);
                    $('#beforeLoginDiv').after(' усешно залогинился</div>');

                } else {
                    loginFailed(data);
                }
            },
            error: loginFailed
        });
    });


</script>

</body>
</html>

